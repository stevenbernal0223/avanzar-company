select clientes.id_Cliente, clientes.nombres, clientes.apellidos, tipo_de_documento.tipo_de_documento,
clientes.numero_de_documento, clientes.lugar_de_expedicion, clientes.telefono, 
clientes.correo_electronico, clientes.direccion_de_residencia, 
concat(beneficiarios.nombres, ' ', beneficiarios.apellidos)nombre_del_beneficiario,
planes.tipo_de_plan, eps.nombre_de_la_EPS, arl.nombre_de_la_ARL, afp.nombre_de_la_AFP,
caja.nombre_de_la_Caja
from clientes
join tipo_de_documento
on clientes.id_tipo=tipo_de_documento.id_tipo
left join beneficiarios
on clientes.id_beneficiario=beneficiarios.id_beneficiario
join planes
on clientes.id_plan=planes.id_plan
left join eps
on clientes.id_eps=eps.id_eps
left join arl
on clientes.id_arl=arl.id_arl
left join afp
on clientes.id_afp=afp.id_afp
left join caja
on clientes.id_caja=caja.id_caja;
