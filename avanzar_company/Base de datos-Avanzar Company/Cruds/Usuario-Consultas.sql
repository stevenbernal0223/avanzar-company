select usuarios.id_Usuario, usuarios.nombres, usuarios.apellidos, tipo_de_documento.tipo_de_documento,
usuarios.numero_de_documento, usuarios.telefono, usuarios.direccion_de_residencia,
usuarios.correo_electronico, rol.tipo_de_rol, usuarios.usuario, usuarios.contraseña
from usuarios
join tipo_de_documento
on usuarios.id_tipo=tipo_de_documento.id_tipo
join rol
on usuarios.id_rol=rol.id_rol;