-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 23-06-2022 a las 04:37:34
-- Versión del servidor: 10.4.24-MariaDB
-- Versión de PHP: 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `avanzarcompany`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `afp`
--

CREATE TABLE `afp` (
  `id_afp` int(11) NOT NULL,
  `nombre_de_la_AFP` varchar(45) NOT NULL,
  `nit` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `afp`
--

INSERT INTO `afp` (`id_afp`, `nombre_de_la_AFP`, `nit`, `telefono`) VALUES
(1, 'Colfondos', '800.149.496-2', '7484888'),
(2, 'porvenir', '800.144 331-3', '7425454'),
(3, 'Skandia', '800.148.514-2', '5923404'),
(4, 'Proteccion', '800.138.188-1', '2307500'),
(5, 'Colpensiones', '900.336.004-7', '4870300');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `arl`
--

CREATE TABLE `arl` (
  `id_arl` int(11) NOT NULL,
  `nombre_de_la_ARL` varchar(45) NOT NULL,
  `nit` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `arl`
--

INSERT INTO `arl` (`id_arl`, `nombre_de_la_ARL`, `nit`, `telefono`) VALUES
(1, 'Positiva', '860.011.153-6', '6502200'),
(2, 'Axa Colpatria', '860.002.184-6', '4235757'),
(3, 'La Equidad', '860.028.415-5', '7460392'),
(4, 'Sura', '890.903.790-5', '4055911'),
(5, 'Colmena	', '800.226.175-3', '3241111'),
(6, 'Seguro Bolivar', '860.002.503-2', '341 00 77'),
(7, 'Mapfre', '891.700.037-9', '6503300');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `caja`
--

CREATE TABLE `caja` (
  `id_caja` int(11) NOT NULL,
  `nombre_de_la_Caja` varchar(45) NOT NULL,
  `nit` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `caja`
--

INSERT INTO `caja` (`id_caja`, `nombre_de_la_Caja`, `nit`, `telefono`) VALUES
(1, 'Cafam', '860.013.570-3', '3077011'),
(2, 'Compensar', '860.066.942-7', '3077001'),
(3, 'Colsubsidio', '860.007.336-1', '7447525'),
(4, 'Asocajas', '860.032.749-5', '323 27 65'),
(5, 'Comfamiliar Camacol', '890.900.840-1', '230 20 00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `id_Cliente` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `numero_de_documento` varchar(45) NOT NULL,
  `lugar_de_expedicion` varchar(45) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `telefono` varchar(20) NOT NULL,
  `correo_electronico` varchar(45) DEFAULT NULL,
  `direccion_de_residencia` varchar(45) NOT NULL,
  `id_plan` int(11) NOT NULL,
  `id_eps` int(11) DEFAULT NULL,
  `id_arl` int(11) DEFAULT NULL,
  `id_afp` int(11) DEFAULT NULL,
  `id_caja` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id_Cliente`, `id_tipo`, `numero_de_documento`, `lugar_de_expedicion`, `nombres`, `apellidos`, `telefono`, `correo_electronico`, `direccion_de_residencia`, `id_plan`, `id_eps`, `id_arl`, `id_afp`, `id_caja`) VALUES
(1, 4, '98021856778', 'Bogota', 'Julian Santiago', 'Jimenez Sosa', '3236851102', 'MariaSosa_15@gmail.com', 'Calle 100 #90c-20', 2, 1, 4, NULL, 2),
(2, 1, '98546231', 'Medellin', 'Laura Angelica', 'Pardo Roa', '3114785263', 'LauAngel2019@gmail.com', 'Calle 142c # 115a -20', 4, 10, 7, 5, 4),
(3, 1, '856423170', 'Bogota', 'Maria Magdalena', 'Herraiz', '3104258694', 'heddossuffepo-7712@gmail.com', 'Calle 10 # 55-51', 1, 7, 5, NULL, NULL),
(4, 1, '962457136', 'Bogota', 'Antonio Javier', 'Monge Lopez', ' 5926035', 'mepezexullau-7113@hotmail.com', 'Calle 28 # 2-27', 3, 4, 5, 1, NULL),
(5, 2, '10000425', 'Venezuela', 'Nataly Lorena', 'Peña Cifuentes', '3214785968', 'cexen23166@outlook.com', 'Calle 10 # 8-07', 1, 5, 1, NULL, NULL),
(6, 4, '10145263214', 'Cali', 'Johan Yesid', 'Rozo Torres', '32489502124', 'America1556@gmail.com', 'Calle 9 # 9 – 62', 5, NULL, 6, NULL, NULL),
(7, 3, '5678491', 'Estados Unidos', 'Ashly Andrea ', 'Buitrago Duarte', '3156247300', 'BuitragoDuarte2020@hotmail.com', 'Calle 105 # 20-51 ', 5, NULL, 2, NULL, NULL),
(8, 1, '1025842736', 'Bogota', 'Lizeth Maria', 'Montero Robayo', '3114752100', 'Lizrobayo2021@gmail.com', 'Calle 157 #36a-10', 3, 8, 4, 5, NULL),
(9, 2, '35472684', 'Ecuador', 'Daniel Celestin', 'Herrera Seguro', '3204756', 'danielh.14.25@gmail.com', 'Cr 56 #116-20', 5, NULL, 2, NULL, NULL),
(10, 4, '1506224561', 'Pasto', 'Tatiana', 'Carvajal Alvarez', '2015867', 'Tatiscarvajal15457@outlook.es', 'Calle 132 #98c-46', 4, 6, 6, NULL, 4),
(11, 1, '1122400311', 'Medellin', 'Carol Elizabeth', 'Roja Vargas', '3126897012', 'vargas-453♠4gmail.com', 'Calle 15sur # 25a-30 ', 2, 6, 1, NULL, 5),
(12, 1, '78546213', 'Cali', 'Hernan ', 'Saul Rosa', '45682137', NULL, 'Diag 15 #90a- 32', 1, 5, 7, NULL, NULL),
(13, 2, '25647820', 'Peru', 'Damaris Edilia', 'Sanchez Porras', '3002530011', 'damaris-p1@hotmail.com', 'Calle 147 #02-12', 4, 9, 1, 2, 4),
(14, 4, '101930024', 'Bogota', 'Katerin Johana', 'Ovalle Pinto', '321478510', 'ktovalle_col@gmail.com', 'Cr 20 #34-03', 5, NULL, 1, NULL, NULL),
(15, 1, '122335411', 'Bogota', 'Luis Camilo', 'Martinez Ramos', '4802531', 'mmilor.amos@outlook.com', 'Calle 130 #36-80', 4, 3, 6, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `eps`
--

CREATE TABLE `eps` (
  `id_eps` int(11) NOT NULL,
  `nombre_de_la_EPS` varchar(45) NOT NULL,
  `nit` varchar(45) NOT NULL,
  `telefono` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `eps`
--

INSERT INTO `eps` (`id_eps`, `nombre_de_la_EPS`, `nit`, `telefono`) VALUES
(1, 'Compensar', '860.066.942-7', '4441234'),
(2, 'EPS Sura', '890.903.790-5', '489 7941'),
(3, 'Cafam Eps', '860.013.570-3 ', '3077011'),
(4, 'Saludcoop', '800.250.119', '6511000 '),
(5, 'Sanitas', '800.251.440-6', '018000940304'),
(6, 'EPS Colsubsidio', '860.007.336-1', '7447525'),
(7, 'Cafesalud E.P.S. S.A.	', '800.140.949-6', '800.140.949-6'),
(8, 'Salud Colmena E.P.S. S.A.	', '800.226.175-3', '3241111'),
(9, 'Coomeva E.P.S.	', '890.300.625-1', '01 8000 930 779'),
(10, 'Cruz Blanca E.P.S. ', '830.009.783-0', '3163620');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planes`
--

CREATE TABLE `planes` (
  `id_plan` int(11) NOT NULL,
  `tipo_de_plan` varchar(45) DEFAULT NULL,
  `servicios` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `planes`
--

INSERT INTO `planes` (`id_plan`, `tipo_de_plan`, `servicios`) VALUES
(1, 'Vital ', 'EPS, ARL'),
(2, 'Basico', 'EPS, ARL, CAJA DE COMPENSACION'),
(3, 'Complementario', 'EPS, ARL, AFP'),
(4, 'Integral', 'EPS, ARL, AFP, CAJA DE COMPENSACION'),
(5, 'Arl', 'ARL');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id_rol` int(11) NOT NULL,
  `tipo_de_rol` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id_rol`, `tipo_de_rol`) VALUES
(1, 'Administrador'),
(2, 'Colaborador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_de_documento`
--

CREATE TABLE `tipo_de_documento` (
  `id_tipo` int(11) NOT NULL,
  `tipo_de_documento` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tipo_de_documento`
--

INSERT INTO `tipo_de_documento` (`id_tipo`, `tipo_de_documento`) VALUES
(1, 'Cedula de ciudadania'),
(2, 'Cedula de extranjeria'),
(3, 'Pasaporte'),
(4, 'Tarjeta de identidad');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_Usuario` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `numero_de_documento` varchar(45) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `telefono` varchar(11) NOT NULL,
  `direccion_de_residencia` varchar(45) NOT NULL,
  `correo_electronico` varchar(45) NOT NULL,
  `usuario` varchar(45) NOT NULL,
  `contraseña` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_Usuario`, `id_rol`, `id_tipo`, `numero_de_documento`, `nombres`, `apellidos`, `telefono`, `direccion_de_residencia`, `correo_electronico`, `usuario`, `contraseña`) VALUES
(1, 1, 1, '1019137797', 'Jimmy Steven ', 'Carrillo Bernal', '3022915468', 'Calle 151c #109a-50', 'stevenbernal0223@gmail.com', 'StevenBernal0223', '1823'),
(2, 2, 1, '1023859678', 'Camilo Andres', 'Ordoñes Chaves', '3215869781', 'Cll 115 #91-25', 'CamiloChaves15', 'CamiloC25', '172839'),
(3, 2, 1, '10175468979', 'Johan Estiven', 'Herrera Godoy', '3225476921', 'Cr 15sur #102a- 58', 'JohanHerrera00@hotmail.com', 'JohanH34', '147258369');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `afp`
--
ALTER TABLE `afp`
  ADD PRIMARY KEY (`id_afp`);

--
-- Indices de la tabla `arl`
--
ALTER TABLE `arl`
  ADD PRIMARY KEY (`id_arl`);

--
-- Indices de la tabla `caja`
--
ALTER TABLE `caja`
  ADD PRIMARY KEY (`id_caja`);

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`id_Cliente`),
  ADD KEY `id_plan` (`id_plan`),
  ADD KEY `id_eps` (`id_eps`),
  ADD KEY `id_arl` (`id_arl`),
  ADD KEY `id_afp` (`id_afp`),
  ADD KEY `id_caja` (`id_caja`),
  ADD KEY `tipo_de_documento` (`id_tipo`),
  ADD KEY `id_tipo` (`id_tipo`);

--
-- Indices de la tabla `eps`
--
ALTER TABLE `eps`
  ADD PRIMARY KEY (`id_eps`);

--
-- Indices de la tabla `planes`
--
ALTER TABLE `planes`
  ADD PRIMARY KEY (`id_plan`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id_rol`);

--
-- Indices de la tabla `tipo_de_documento`
--
ALTER TABLE `tipo_de_documento`
  ADD PRIMARY KEY (`id_tipo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_Usuario`),
  ADD KEY `tipo_de_documento` (`id_tipo`),
  ADD KEY `id_rol` (`id_rol`),
  ADD KEY `id_tipo` (`id_tipo`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `afp`
--
ALTER TABLE `afp`
  MODIFY `id_afp` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `arl`
--
ALTER TABLE `arl`
  MODIFY `id_arl` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `caja`
--
ALTER TABLE `caja`
  MODIFY `id_caja` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `id_Cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `eps`
--
ALTER TABLE `eps`
  MODIFY `id_eps` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `planes`
--
ALTER TABLE `planes`
  MODIFY `id_plan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_Usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD CONSTRAINT `clientes_ibfk_1` FOREIGN KEY (`id_plan`) REFERENCES `planes` (`id_plan`),
  ADD CONSTRAINT `clientes_ibfk_2` FOREIGN KEY (`id_eps`) REFERENCES `eps` (`id_eps`),
  ADD CONSTRAINT `clientes_ibfk_3` FOREIGN KEY (`id_arl`) REFERENCES `arl` (`id_arl`),
  ADD CONSTRAINT `clientes_ibfk_4` FOREIGN KEY (`id_afp`) REFERENCES `afp` (`id_afp`),
  ADD CONSTRAINT `clientes_ibfk_5` FOREIGN KEY (`id_caja`) REFERENCES `caja` (`id_caja`),
  ADD CONSTRAINT `clientes_ibfk_8` FOREIGN KEY (`id_tipo`) REFERENCES `tipo_de_documento` (`id_tipo`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id_rol`),
  ADD CONSTRAINT `usuarios_ibfk_3` FOREIGN KEY (`id_tipo`) REFERENCES `tipo_de_documento` (`id_tipo`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
