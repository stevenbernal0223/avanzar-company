package com.avanzar.avanzarcompany;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvanzarcompanyApplication {

	public static void main(String[] args) {
		SpringApplication.run(AvanzarcompanyApplication.class, args);
	}

}
