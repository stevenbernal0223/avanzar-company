package com.avanzar.avanzarcompany.commons;

import java.io.Serializable;
import java.util.List;



public interface GenericService<T, ID extends Serializable> {
	
	public T guardar(T entidad);
	
	public void eliminar(ID id);
	
	public T consultar(ID id);
	
	public List<T> listar();
	
	
	
	

}

