package com.avanzar.avanzarcompany.commons;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public abstract class GenericServiceImpl<T, ID extends Serializable> implements GenericService<T, ID> {
	
	@Override
	public T guardar(T entidad) {
		return getDao().save(entidad);
	}
	
	@Override
	public void eliminar(ID id) {
		getDao().deleteById(id);
	}
	
	@Override
	public T consultar(ID id) {
		Optional<T> objeto = getDao().findById(id);
		if(objeto.isPresent()) {
			return objeto.get();
		}	
		return null;
	}

	@Override
	public List<T> listar() {
		List<T> listaRetornada = new ArrayList<>();
		getDao().findAll().forEach(obj -> listaRetornada.add(obj) );
		
		return listaRetornada;
		
	}
	

	public abstract CrudRepository<T, ID> getDao();

}
