package com.avanzar.avanzarcompany.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.avanzar.avanzarcompany.model.Clientes;
import com.avanzar.avanzarcompany.model.Usuarios;

import com.avanzar.avanzarcompany.service.ClientesService;

import com.avanzar.avanzarcompany.service.UsuariosService;

@Controller
public class ActualizacionClientesController {

	@Autowired
    private ClientesService clientesService;

   	@Autowired
    private UsuariosService usuarioService;

    private Usuarios usuarios;
    private Clientes cliente;
    



    private static final Logger log = LogManager.getLogger(ClientesController.class);

    @GetMapping("/actualizarCliente/{idU}/{id}")
    public String cedulaC(@PathVariable int idU, Model model, @PathVariable int id, Model model3) {
    	usuarios = new Usuarios();
        log.info("idU" + id);
        log.info("id" + id);
        usuarios = usuarioService.consultar(idU);
        model.addAttribute(usuarios);
        cliente = new Clientes();
    	cliente = clientesService.consultar(id);
    	model3.addAttribute(cliente);
    	return "ActualizacionClientes";
    }
    
    @PostMapping("/clienteEliminar")
    public String eliminarCliente(int id, RedirectAttributes redirectAttribute){
        clientesService.eliminar(id);
        if(clientesService.consultar(id) != null){
            redirectAttribute
                    .addFlashAttribute("mensaje", "Fallo al eliminar")
                    .addFlashAttribute("clase", "alert alert-success");
        }else{
            redirectAttribute
                    .addFlashAttribute("mensaje", "Informacion de cliente eliminada correctamente.")
                    .addFlashAttribute("clase", "alert alert-danger" );
        }
        return "redirect:/ActualizacionV/"+usuarios.getId_Usuario();	

}
    
    @PostMapping("/actCliente")
    public String actCliente(@ModelAttribute("clientes")Clientes clientes, @ModelAttribute("id") int id, Model model, RedirectAttributes redirectAttribute){
        model.addAttribute("clientes",clientes);
        model.addAttribute("id", id);
        clientesService.actualizarCliente(clientes.getTelefono(), clientes.getCorreo_electronico(), clientes.getDireccion_de_residencia(), id);
        redirectAttribute
        .addFlashAttribute("mensaje", "Informacion de cliente actualizada correctamente")
        .addFlashAttribute("clase", "alert alert-success");
        return "redirect:/ActualizacionV/"+usuarios.getId_Usuario();
    }

}
