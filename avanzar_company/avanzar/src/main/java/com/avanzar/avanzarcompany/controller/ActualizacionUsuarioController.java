package com.avanzar.avanzarcompany.controller;

import com.avanzar.avanzarcompany.model.Rol;
import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.RolService;
import com.avanzar.avanzarcompany.service.UsuariosService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ActualizacionUsuarioController {

    @Autowired
    private UsuariosService usuarioService;
    
    @Autowired
    private RolService rolService;

    private Rol rol;
    private Usuarios usuarios;
   
      
    private Usuarios user2;
    


    private static final Logger log = LogManager.getLogger(UsuariosController.class);
    
    @GetMapping("/actualizarUsuario/{idP}/{id}")
    public String cedula(@PathVariable int idP, @PathVariable int id ,Model model,Model model2){
        usuarios = new Usuarios();
        usuarios = usuarioService.consultar(idP);
        model.addAttribute(usuarios);
        log.info("idP" + idP);
        log.info("id" + id);    
        user2 = usuarioService.consultar(id);
        model2.addAttribute("user2", user2);
        return "ActualizacionUsuarios";
    }
    
    
        @PostMapping("/usuarioEliminar")
        public String eliminarUsuario(int id, RedirectAttributes redirectAttribute){
        	usuarioService.eliminar(id);
            if(usuarioService.consultar(id) != null){
                redirectAttribute
                        .addFlashAttribute("mensaje", "Fallo al eliminar")
                        .addFlashAttribute("clase", "alert alert-success");
            }else{
                redirectAttribute
                        .addFlashAttribute("mensaje", "Informacion de usuario eliminada correctamente")
                        .addFlashAttribute("clase", "alert alert-danger" );
            }
            return "redirect:/ActualizacionV/"+usuarios.getId_Usuario();	

    
}
        
        @PostMapping("/actUsuario")
        public String actUsuario(@ModelAttribute("usuarios")Usuarios usuarios2, @ModelAttribute("id") int id, Model model, RedirectAttributes redirectAttribute){
            model.addAttribute("usuarios",usuarios2);
            model.addAttribute("id", id);
            usuarioService.actualizarUsuario(usuarios2.getTelefono(), usuarios2.getCorreo_electronico(), usuarios2.getDireccion_de_residencia(), usuarios2.getUsuario(), usuarios2.getContraseña(), id);
            redirectAttribute
            .addFlashAttribute("mensaje", "Informacion de usuario actualizada correctamente")
            .addFlashAttribute("clase", "alert alert-success");
            return "redirect:/ActualizacionV/"+usuarios.getId_Usuario();
        }
}
