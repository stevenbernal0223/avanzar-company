package com.avanzar.avanzarcompany.controller;

import com.avanzar.avanzarcompany.model.Afp;
import com.avanzar.avanzarcompany.service.AfpService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/afp")
public class AfpController  {

    @Autowired
    private AfpService service;

    private static final Logger log = LogManager.getLogger(AfpController.class);

    @GetMapping("/listarAfps")
    public List<Afp> listarAfps() {
        List<Afp> afps = service.listar();
        log.info(afps);
        return afps;
    }

    //?id=
    @GetMapping("/consultarAfp")
    public Afp consultarAfp(Integer id) {
        Afp afp = service.consultar(id);
        log.info(afp);
        return afp;
    }


    @PostMapping("/crearAfp")
    public void crearAfp(@RequestBody Afp afp){
        service.guardar(afp);
    }

    @DeleteMapping("/eliminarAfp")
    public void eliminarAfp(Integer id){
        service.eliminar(id);
    }

    
    @PutMapping("/actualizarAfp")
    public String actualizarAfp(int id,String nombrec, String nitc, String telefonoc) {
    	Afp afpc = service.consultar(id);
    	if(afpc == null) {
    		return "La Afp ingresada no existe";
    	}
    	
    	afpc.setNit(nitc);
    	afpc.setNombre_de_la_AFP(nombrec);
    	afpc.setTelefono(telefonoc);
    	service.guardar(afpc);
    	return "El Campo ARL ha sido actualizado correctamente";
    }

}
