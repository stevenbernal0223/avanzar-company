package com.avanzar.avanzarcompany.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.avanzar.avanzarcompany.model.Arl;
import com.avanzar.avanzarcompany.service.ArlService;



@RestController
@RequestMapping(value = "/arl")
public class ArlController {
	
	@Autowired
    private ArlService service;

    private static final Logger log = LogManager.getLogger(ArlController.class);

    @GetMapping("/listarArl")
    public List<Arl> listarArl() {
        List<Arl> arl = service.listar();
        log.info(arl);
        return arl;
    }

  //?id=
    @GetMapping("/consultarArl")
    public Arl consultarArl(Integer id) {
        Arl arl = service.consultar(id);
        log.info(arl);
        return arl;
    }

    @PostMapping("/crearArl")
    public void crearArl(@RequestBody Arl arl){
        service.guardar(arl);
    }

    @DeleteMapping("/eliminarArl")
    public void eliminarArl(Integer id){
        service.eliminar(id);
    }
    
    @PutMapping("/actualizarArl")
    public String actualizarArl(int id,String nombrec, String nitc, String telefonoc) {
    	Arl arlc = service.consultar(id);
    	if(arlc == null) {
    		return "La ARL ingresada no existe";
    	}
    	
    	arlc.setNit(nitc);
    	arlc.setNombre_de_la_ARL(nombrec);
    	arlc.setTelefono(telefonoc);
    	service.guardar(arlc);
    	return "El Campo ARL ha sido actualizado correctamente";
    }
}
