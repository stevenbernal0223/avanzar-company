package com.avanzar.avanzarcompany.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.avanzar.avanzarcompany.model.Caja;
import com.avanzar.avanzarcompany.service.CajaService;

import java.util.List;


@RestController
@RequestMapping(value = "/caja")
public class CajaController {
	
	@Autowired
    private CajaService service;

    private static final Logger log = LogManager.getLogger(CajaController.class);

    @GetMapping("/listarCaja")
    public List<Caja> listarCaja() {
        List<Caja> caja = service.listar();
        log.info(caja);
        return caja;
    }
    
    //?id=
    @GetMapping("/consultarCaja")
    public Caja consultarCaja(Integer id) {
        Caja caja = service.consultar(id);
        log.info(caja);
        return caja;
    }

    @PostMapping("/crearCaja")
    public void crearCaja(@RequestBody Caja caja){
        service.guardar(caja);
    }

    @DeleteMapping("/eliminarCaja")
    public void eliminarCaja(Integer id){
        service.eliminar(id);
    }
    
    @PutMapping("/actualizarCaja")
    public String actualizarCaja(int id,String nombrec, String nitc, String telefonoc) {
    	Caja cajac = service.consultar(id);
    	if(cajac == null) {
    		return "La Caja ingresada no existe";
    	}
    	
    	cajac.setNit(nitc);
    	cajac.setNombre_de_la_Caja(nombrec);
    	cajac.setTelefono(telefonoc);
    	service.guardar(cajac);
    	return "El Campo Caja a sido actualizado correctamente";
    }
}
