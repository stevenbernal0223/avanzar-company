package com.avanzar.avanzarcompany.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.avanzar.avanzarcompany.model.Clientes;
import com.avanzar.avanzarcompany.service.ClientesService;

import java.util.List;

@RestController
@RequestMapping(value = "/clientes")
public class ClientesController {
	
	@Autowired
	private ClientesService service;
    private static final Logger log = LogManager.getLogger(ClientesController.class);
    
    
	@GetMapping("/listarClientes")
    public List<Clientes> listarClientes() {
        List<Clientes> clientes = service.listar();
        log.info(clientes);
        return clientes;
    }
	
	//?id=
    @GetMapping("/consultarClientes")
    public Clientes consultarClientes(Integer id) {
        Clientes clientes = service.consultar(id);
        log.info(clientes);
        return clientes;
    }

    @PostMapping("/crearClientes")
    public void crearClientes(@RequestBody Clientes clientes){
        service.guardar(clientes);
    }

    @DeleteMapping("/eliminarClientes")
    public void eliminarClientes(Integer id){
        service.eliminar(id);
    }
    
    @PutMapping("/actualizarClientes")
    public String actualizarClientes
    (int id, String numeroc, String lugarc, String nombresc, String apellidosc,
    		String telefonoc, String correoc, String direccionc) {
    	Clientes clientec = service.consultar(id);
    	if(clientec == null) {
    		return "El cliente ingresado no existe";
    	}
    	
    	clientec.setNumero_de_documento(numeroc);
    	clientec.setLugar_de_expedicion(lugarc);
    	clientec.setNombres(nombresc);
    	clientec.setApellidos(apellidosc);
    	clientec.setTelefono(telefonoc);
    	clientec.setCorreo_electronico(correoc);
    	clientec.setDireccion_de_residencia(direccionc);
    	service.guardar(clientec);
    	return "El campo cliente ha sido actualizado correctamente";
    }

    }
