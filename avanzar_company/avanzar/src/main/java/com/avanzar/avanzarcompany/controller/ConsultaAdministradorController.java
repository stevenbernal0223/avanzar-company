package com.avanzar.avanzarcompany.controller;

import com.avanzar.avanzarcompany.model.Clientes;
import com.avanzar.avanzarcompany.model.Rol;
import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.ClientesService;
import com.avanzar.avanzarcompany.service.RolService;
import com.avanzar.avanzarcompany.service.UsuariosService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@Controller
public class ConsultaAdministradorController {

    @Autowired
    private UsuariosService usuarioService;

    @Autowired
    private RolService rolService;
    
    @Autowired
    private ClientesService clientesService;

    private static final Logger log = LogManager.getLogger(UsuariosController.class);

    Integer idUsuario;
    
    @GetMapping("/consultaAdministrador1/{id}")
    public String index(@PathVariable int id, Model model){
        idUsuario = id;
        Usuarios usuarios = new Usuarios();
        usuarios = usuarioService.consultar(id);
        Rol rol = rolService.consultar(usuarios.getRol().getId_rol());
        model.addAttribute(usuarios);
        model.addAttribute(rol);
        return "consultaAdministrador";
    }

    @GetMapping("/registrar")
    public String registroUsuario(Model model){
        Usuarios usuarios = new Usuarios();
        usuarios = usuarioService.consultar(idUsuario);
        model.addAttribute(usuarios);
        return "redirect:/registroUsuario/"+usuarios.getId_Usuario();
    }
    
    @GetMapping("/registrarClientes")
    public String registroClientes(Model model){
        Usuarios usuarios = new Usuarios();
        usuarios = usuarioService.consultar(idUsuario);
        model.addAttribute(usuarios);
        return "redirect:/registroCliente/"+usuarios.getId_Usuario();
    }
    
    @GetMapping("/Actualizacion")
    public String Actualizacion (Model model){
        Usuarios usuarios = new Usuarios();
        usuarios = usuarioService.consultar(idUsuario);
        Rol rol = rolService.consultar(usuarios.getRol().getId_rol());
        model.addAttribute(usuarios);
        model.addAttribute(rol);
        return "redirect:/Actualizacion/"+usuarios.getId_Usuario();
    }
    
    @GetMapping("/paginaEps")
    public String EPS (Model model){
        Usuarios usuarios = new Usuarios();
        usuarios = usuarioService.consultar(idUsuario);
        Rol rol = rolService.consultar(usuarios.getRol().getId_rol());
        model.addAttribute(usuarios);
        model.addAttribute(rol);
        return "redirect:/paginaEps/"+usuarios.getId_Usuario();
    }
    

    @GetMapping("/paginaArl")
    public String Arl (Model model){
        Usuarios usuarios = new Usuarios();
        usuarios = usuarioService.consultar(idUsuario);
        Rol rol = rolService.consultar(usuarios.getRol().getId_rol());
        model.addAttribute(usuarios);
        model.addAttribute(rol);
        return "redirect:/paginaArl/"+usuarios.getId_Usuario();
    }
    
    @GetMapping("/paginaAfp")
    public String Afp (Model model){
        Usuarios usuarios = new Usuarios();
        usuarios = usuarioService.consultar(idUsuario);
        Rol rol = rolService.consultar(usuarios.getRol().getId_rol());
        model.addAttribute(usuarios);
        model.addAttribute(rol);
        return "redirect:/paginaAfp/"+usuarios.getId_Usuario();
    }
    
    @GetMapping("/paginaCaja")
    public String Caja(Model model){
        Usuarios usuarios = new Usuarios();
        usuarios = usuarioService.consultar(idUsuario);
        Rol rol = rolService.consultar(usuarios.getRol().getId_rol());
        model.addAttribute(usuarios);
        model.addAttribute(rol);
        return "redirect:/paginaCaja/"+usuarios.getId_Usuario();
      
    }
    
    

    @PostMapping("/consultarCedula")
    public String consultarUsuario(@ModelAttribute("cedula") String cedula, Model model, BindingResult bindingResult,RedirectAttributes redirectAttribute) {
    	if (bindingResult.hasErrors()) {
            return "consulta";
        }
    	Usuarios usuarios = new Usuarios();
    	model.addAttribute("cedula",cedula);
    	log.info(usuarios.getUsuario());
    	usuarios=usuarioService.consultaUsuario(cedula);
    	if(usuarioService.consultaUsuario(cedula) != null) {
        	redirectAttribute
            .addFlashAttribute("mensaje2", "Usuario existente")
            .addFlashAttribute("clase2", "alert alert-success");
        	usuarioService.consultaUsuario(cedula);
        	return "redirect:/consultaCedula/"+idUsuario+"/"+usuarios.getId_Usuario();
    	}else {
        	redirectAttribute
            .addFlashAttribute("mensaje3", "El usuario ingresado no existe o no es correcto.")
            .addFlashAttribute("clase3", "alert alert-danger" );
        	return"redirect:/consultaAdministradorV/"+idUsuario;
        	  }
    }
     
    
    

    @PostMapping("/consultarCliente")
    public String ConsultarCliente(@ModelAttribute("cedulaC") String cedulaC, Model model, BindingResult bindingResult,RedirectAttributes redirectAttribute2) {
    	if(bindingResult.hasErrors()) {
    		return "consulta2";
    	}
    	Clientes cliente = new Clientes();
    	model.addAttribute("cedulaC",cedulaC);
    	log.info(cliente.getId_Cliente());
    	cliente = clientesService.consultaClientes(cedulaC);
    	if(clientesService.consultaClientes(cedulaC) != null) {
    		redirectAttribute2
            .addFlashAttribute("mensaje2", "Usuario existente")
            .addFlashAttribute("clase2", "alert alert-success");
    		clientesService.consultaClientes(cedulaC);
    		return "redirect:/consultaCliente/"+idUsuario+"/"+cliente.getId_Cliente();
    	} else {
    		redirectAttribute2
            .addFlashAttribute("mensaje3", "El Cliente ingresado no existe o es no correcto.")
            .addFlashAttribute("clase3", "alert alert-danger" );
        	return"redirect:/consultaAdministradorV/"+idUsuario;	
    	}
    }
    
    @GetMapping("/consultaAdministradorV/{id}")
    public String vuelta(@PathVariable int id, Model model){
        idUsuario = id;
        Usuarios usuarios = new Usuarios();
        usuarios = usuarioService.consultar(id);
        Rol rol = rolService.consultar(usuarios.getRol().getId_rol());
        model.addAttribute(usuarios);
        model.addAttribute(rol);
        return "consultaAdministrador";
}
}
