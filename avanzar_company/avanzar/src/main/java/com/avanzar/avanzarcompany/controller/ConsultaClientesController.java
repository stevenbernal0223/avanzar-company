package com.avanzar.avanzarcompany.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.avanzar.avanzarcompany.model.Clientes;

import com.avanzar.avanzarcompany.model.Usuarios;

import com.avanzar.avanzarcompany.service.ClientesService;

import com.avanzar.avanzarcompany.service.UsuariosService;

@Controller
public class ConsultaClientesController {

	@Autowired
    private ClientesService clientesService;

   	@Autowired
    private UsuariosService usuarioService;

    private Usuarios usuarios;
    private Clientes cliente;


    private static final Logger log = LogManager.getLogger(ClientesController.class);

    @GetMapping("/consultaCliente/{idU}/{id}")
    public String cedulaC(@PathVariable int idU, Model model, @PathVariable int id, Model model2) {
    	usuarios = new Usuarios();
        log.info("idU" + id);
        log.info("id" + id);
        usuarios = usuarioService.consultar(idU);
        model.addAttribute(usuarios);
        cliente = new Clientes();
    	cliente = clientesService.consultar(id);
    	model2.addAttribute(cliente);
    	return "ConsultaDatosClientes";
    }


}
