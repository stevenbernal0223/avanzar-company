package com.avanzar.avanzarcompany.controller;

import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.UsuariosService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ConsultaUsuarioController {
 
	@Autowired
    private UsuariosService usuarioService;
    
    private Usuarios usuarios;
    
    private Usuarios user2;
    

    private static final Logger log = LogManager.getLogger(UsuariosController.class);

    @GetMapping("/consultaCedula/{idP}/{id}")
    public String cedula(@PathVariable int idP, @PathVariable int id ,Model model,Model model2){
        usuarios = new Usuarios();
        usuarios = usuarioService.consultar(idP);
        model.addAttribute(usuarios);
        log.info("idP" + idP);
        log.info("id" + id);    
        user2 = usuarioService.consultar(id);
        model2.addAttribute("user2", user2);
        return "ConsultaDatosUsuarios";
    }
    
    
}