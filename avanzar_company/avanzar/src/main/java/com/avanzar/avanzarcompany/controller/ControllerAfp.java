package com.avanzar.avanzarcompany.controller;

import com.avanzar.avanzarcompany.model.Afp;
import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.AfpService;
import com.avanzar.avanzarcompany.service.UsuariosService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ControllerAfp {

    @Autowired
    private UsuariosService usuarioService;

    @Autowired
    private AfpService afpService;

    private Usuarios usuarios;

    Integer idRol;

    private static final Logger log = LogManager.getLogger(UsuariosController.class);

    @GetMapping("/paginaAfp/{id}")
    public String AFP(@PathVariable int id,Model model, Model model4){
        usuarios = new Usuarios();
        usuarios = usuarioService.consultar(id);
        idRol = id;
        model.addAttribute(usuarios);
        model4.addAttribute("afps", afpService.listar());
        return "servicioAFP";
    }


    @PostMapping("/afpEliminar")
    public String eliminarAfp(int id, RedirectAttributes redirectAttribute){
        afpService.eliminar(id);
        if(afpService.consultar(id) != null){
            redirectAttribute
                    .addFlashAttribute("mensaje", "Eliminado correctamente")
                    .addFlashAttribute("clase", "alert alert-success");
        }else{
            redirectAttribute
                    .addFlashAttribute("mensaje", "Eliminado correctamente.")
                    .addFlashAttribute("clase", "alert alert-danger" );
        }
        return "redirect:/paginaAfp/"+idRol;
    }

    @PostMapping("/regAfp")
    public String registrarAfpP(){
        return "redirect:/registrarAfp/";
    }

    @GetMapping("/registrarAfp")
    public String registrarEps(Model model){
        Afp afp = new Afp();
        model.addAttribute(afp);
        model.addAttribute(usuarios);
        return "RegistroAFP";
    }

    @PostMapping("/registroAfpNuevo")
    public String regitroNAfp(@ModelAttribute("afp")Afp afp, Model model){
        model.addAttribute("afp",afp);
        afpService.guardar(afp);
        return "redirect:/paginaAfp/"+idRol;
    }

    @PostMapping("/actAfp")
    public String actualizarAfpP(int id){

        return "redirect:/actualizarAfp/"+id;
    }

    @GetMapping("/actualizarAfp/{id}")
    public String actualizarAfp(@PathVariable int id,Model model){
        Afp afp = new Afp();
        afp = afpService.consultar(id);
        model.addAttribute(afp);
        model.addAttribute(usuarios);
        return "ActualizaAFP";
    }

    @PostMapping("/actualizarAFPS")
    public String actualizarAFPS(@ModelAttribute("afp")Afp afp, @ModelAttribute("idE") int idE, Model model, RedirectAttributes redirectAttribute){
        model.addAttribute("afp",afp);
        model.addAttribute("idE", idE);
        afpService.actualizarAfp(afp.getTelefono(), idE);
        return "redirect:/paginaAfp/"+idRol;
    }


    
    

}