package com.avanzar.avanzarcompany.controller;

import com.avanzar.avanzarcompany.model.Arl;
import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.ArlService;
import com.avanzar.avanzarcompany.service.UsuariosService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ControllerArl {

    @Autowired
    private UsuariosService usuarioService;

    @Autowired
    private ArlService arlService;

    private Usuarios usuarios;

    Integer idRol;

    private static final Logger log = LogManager.getLogger(UsuariosController.class);

 
    @GetMapping("/paginaArl/{id}")
    public String Arl(@PathVariable int id,Model model, Model model3){
        usuarios = new Usuarios();
        usuarios = usuarioService.consultar(id);
        idRol = id;
        model.addAttribute(usuarios);
        model3.addAttribute("arls", arlService.listar());
        return "servicioARL";
    }
  

    @PostMapping("/arlEliminar")
    public String eliminarArl(int id, RedirectAttributes redirectAttribute){
        arlService.eliminar(id);
        if(arlService.consultar(id) != null){
            redirectAttribute
                    .addFlashAttribute("mensaje", "Eliminado correctamente")
                    .addFlashAttribute("clase", "alert alert-success");
        }else{
            redirectAttribute
                    .addFlashAttribute("mensaje", "Eliminado correctamente.")
                    .addFlashAttribute("clase", "alert alert-danger" );
        }
        return "redirect:/paginaArl/"+idRol;
    }

    @PostMapping("/regArl")
    public String registrarArlP(){
        return "redirect:/registrarArl/";
    }

    @GetMapping("/registrarArl")
    public String registrarArl(Model model){
        Arl arl = new Arl();
        model.addAttribute(arl);
        model.addAttribute(usuarios);
        return "RegistroARL";
    }

    @PostMapping("/registroArlNuevo")
    public String regitroNArl(@ModelAttribute("arl")Arl arl, Model model){
        model.addAttribute("arl",arl);
        arlService.guardar(arl);
        return "redirect:/paginaArl/"+idRol;
    }

    @PostMapping("/actArl")
    public String actualizarArlP(int id){

        return "redirect:/actualizarArl/"+id;
    }

    @GetMapping("/actualizarArl/{id}")
    public String actualizarArl(@PathVariable int id,Model model){
        Arl arl = new Arl();
        arl = arlService.consultar(id);
        model.addAttribute(arl);
        model.addAttribute(usuarios);
        return "ActualizaARL";
    }

    @PostMapping("/actualizarARLS")
    public String actualizarARLS(@ModelAttribute("arl")Arl arl, @ModelAttribute("idE") int idE, Model model, RedirectAttributes redirectAttribute){
        model.addAttribute("arl",arl);
        model.addAttribute("idE", idE);
        arlService.actualizarArl(arl.getTelefono(), idE);
        return "redirect:/paginaArl/"+idRol;
    }


    
    

}

    
