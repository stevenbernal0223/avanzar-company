package com.avanzar.avanzarcompany.controller;

import com.avanzar.avanzarcompany.model.Caja;
import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.CajaService;
import com.avanzar.avanzarcompany.service.UsuariosService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ControllerCaja {

    @Autowired
    private UsuariosService usuarioService;

    @Autowired
    private CajaService cajaService;

    private Usuarios usuarios;

    Integer idRol;

    private static final Logger log = LogManager.getLogger(UsuariosController.class);

 
    @GetMapping("/paginaCaja/{id}")
    public String Caja(@PathVariable int id,Model model, Model model5){
        usuarios = new Usuarios();
        usuarios = usuarioService.consultar(id);
        idRol = id;
        model.addAttribute(usuarios);
        model5.addAttribute("cajas", cajaService.listar());
        return "servicioCAJA";
    }
  

    @PostMapping("/cajaEliminar")
    public String eliminarCaja(int id, RedirectAttributes redirectAttribute){
        cajaService.eliminar(id);
        if(cajaService.consultar(id) != null){
            redirectAttribute
                    .addFlashAttribute("mensaje", "Eliminado correctamente")
                    .addFlashAttribute("clase", "alert alert-success");
        }else{
            redirectAttribute
                    .addFlashAttribute("mensaje", "Eliminado correctamente.")
                    .addFlashAttribute("clase", "alert alert-danger" );
        }
        return "redirect:/paginaCaja/"+idRol;
    }

    @PostMapping("/regCaja")
    public String registrarCajaP(){
        return "redirect:/registrarCaja/";
    }

    @GetMapping("/registrarCaja")
    public String registrarCaja(Model model){
        Caja caja = new Caja();
        model.addAttribute(caja);
        model.addAttribute(usuarios);
        return "RegistroCAJA";
    }

    @PostMapping("/registroCajaNuevo")
    public String regitroNCaja(@ModelAttribute("caja")Caja caja, Model model){
        model.addAttribute("caja",caja);
        cajaService.guardar(caja);
        return "redirect:/paginaCaja/"+idRol;
    }

    @PostMapping("/actCaja")
    public String actualizarCajaP(int id){

        return "redirect:/actualizarCaja/"+id;
    }

    @GetMapping("/actualizarCaja/{id}")
    public String actualizarCaja(@PathVariable int id,Model model){
        Caja caja = new Caja();
        caja = cajaService.consultar(id);
        model.addAttribute(caja);
        model.addAttribute(usuarios);
        return "ActualizaCAJA";
    }

    @PostMapping("/actualizarCAJAS")
    public String actualizarCAJAS(@ModelAttribute("caja")Caja caja, @ModelAttribute("idE") int idE, Model model, RedirectAttributes redirectAttribute){
        model.addAttribute("caja",caja);
        model.addAttribute("idE", idE);
        cajaService.actualizarCaja(caja.getTelefono(), idE);
        return "redirect:/paginaCaja/"+idRol;
    }


    
    

}

    
