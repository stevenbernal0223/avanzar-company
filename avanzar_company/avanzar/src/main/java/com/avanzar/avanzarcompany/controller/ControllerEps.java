package com.avanzar.avanzarcompany.controller;

import com.avanzar.avanzarcompany.model.Eps;
import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.EpsService;
import com.avanzar.avanzarcompany.service.UsuariosService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class ControllerEps {

    @Autowired
    private UsuariosService usuarioService;

    @Autowired
    private EpsService epsService;

    private Usuarios usuarios;

    Integer idRol;

    private static final Logger log = LogManager.getLogger(UsuariosController.class);

    @GetMapping("/paginaEps/{id}")
    public String EPS(@PathVariable int id,Model model, Model model2){
        usuarios = new Usuarios();
        usuarios = usuarioService.consultar(id);
        idRol = id;
        model.addAttribute(usuarios);
        model2.addAttribute("epss", epsService.listar());
        return "servicioEPS";
    }


    @PostMapping("/epsEliminar")
    public String eliminarEps(int id, RedirectAttributes redirectAttribute){
        epsService.eliminar(id);
        if(epsService.consultar(id) != null){
            redirectAttribute
                    .addFlashAttribute("mensaje", "Eliminado correctamente")
                    .addFlashAttribute("clase", "alert alert-success");
        }else{
            redirectAttribute
                    .addFlashAttribute("mensaje", "Eliminado correctamente.")
                    .addFlashAttribute("clase", "alert alert-danger" );
        }
        return "redirect:/paginaEps/"+idRol;
    }

    @PostMapping("/regEps")
    public String registrarEpsP(){
        return "redirect:/registrarEps/";
    }

    @GetMapping("/registrarEps")
    public String registrarEps(Model model){
        Eps eps = new Eps();
        model.addAttribute(eps);
        model.addAttribute(usuarios);
        return "RegistroEPS";
    }

    @PostMapping("/registroEpsNuevo")
    public String regitroNEps(@ModelAttribute("eps")Eps eps, Model model){
        model.addAttribute("eps",eps);
        epsService.guardar(eps);
        return "redirect:/paginaEps/"+idRol;
    }

    @PostMapping("/actEps")
    public String actualizarEpsP(int id){

        return "redirect:/actualizarEps/"+id;
    }

    @GetMapping("/actualizarEps/{id}")
    public String actualizarEps(@PathVariable int id,Model model){
        Eps eps = new Eps();
        eps = epsService.consultar(id);
        model.addAttribute(eps);
        model.addAttribute(usuarios);
        return "ActualizaEPS";
    }

    @PostMapping("/actualizarEPSS")
    public String actualizarEPSS(@ModelAttribute("eps")Eps eps, @ModelAttribute("idE") int idE, Model model, RedirectAttributes redirectAttribute){
        model.addAttribute("eps",eps);
        model.addAttribute("idE", idE);
        epsService.actualizarEps(eps.getTelefono(), idE);
        return "redirect:/paginaEps/"+idRol;
    }


    
    

}