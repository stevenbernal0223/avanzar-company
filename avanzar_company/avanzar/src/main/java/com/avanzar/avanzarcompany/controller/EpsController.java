package com.avanzar.avanzarcompany.controller;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import com.avanzar.avanzarcompany.model.Eps;
import com.avanzar.avanzarcompany.service.EpsService;

@RestController
@RequestMapping(value = "/eps")
public class EpsController {
	
	@Autowired
    private EpsService service;

    private static final Logger log = LogManager.getLogger(EpsController.class);

    @GetMapping("/listarEps")
    public List<Eps> listarEps() {
        List<Eps> eps = service.listar();
        log.info(eps);
        return eps;
    }
    
  //?id=
    @GetMapping("/consultarEps")
    public Eps consultarEps(Integer id) {
        Eps eps = service.consultar(id);
        log.info(eps);
        return eps;
    }

    @PostMapping("/crearEps")
    public void crearEps(@RequestBody Eps eps){
        service.guardar(eps);
    }

    @DeleteMapping("/eliminarEps")
    public void eliminarEps(Integer id){
        service.eliminar(id);
    }

    @PutMapping("/actualizarEps")
    public String actualizarEps(int id,String nombrec, String nitc, String telefonoc) {
    	Eps epsc = service.consultar(id);
    	if(epsc == null) {
    		return "La EPS ingresada no existe";
    	}
    	
    	epsc.setNit(nitc);
    	epsc.setNombre_de_la_EPS(nombrec);
    	epsc.setTelefono(telefonoc);
    	service.guardar(epsc);
    	return "El Campo EPS ha sido actualizado correctamente";
    }
    
}
	