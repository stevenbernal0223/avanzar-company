package com.avanzar.avanzarcompany.controller;

import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.UsuariosService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class IndexController {

    @Autowired
    private UsuariosService usuarioService;

    private static final Logger log = LogManager.getLogger(UsuariosController.class);

    @GetMapping("/")
    public String index(Model model){
        Usuarios usuarios = new Usuarios();
        model.addAttribute(usuarios);
        return "index";
    }


    @PostMapping("/login")
    public String logeo(@ModelAttribute("usuarios") Usuarios usuarios, BindingResult bindingResult, Model model, RedirectAttributes redirectAttribute) {
        if (bindingResult.hasErrors()) {
            return "index";
        }
        model.addAttribute("usuarios", usuarios);
        log.info(usuarios.getUsuario());
        log.info(usuarios.getContraseña());
        if(usuarioService.consultaLogin(usuarios.getUsuario(),usuarios.getContraseña()) != null){
            redirectAttribute
                    .addFlashAttribute("mensaje", "Agregado correctamente")
                    .addFlashAttribute("clase", "alert alert-success");
            int id =  usuarioService.consultaLogin(usuarios.getUsuario(),usuarios.getContraseña()).getId_Usuario();
            return "redirect:/consultaAdministrador1/"+id;
        }else{
            redirectAttribute
                    .addFlashAttribute("mensaje", "El usuario o la contraseña ingresados no existe o no son correctos.")
                    .addFlashAttribute("clase", "alert alert-danger" );
            return "redirect:/";
       }

    }

    @PostMapping("/logout")
    public String logout(Model model){
        Usuarios usuarios = new Usuarios();
        model.addAttribute(usuarios);
        return "redirect:/";
    }


}
