package com.avanzar.avanzarcompany.controller;


import com.avanzar.avanzarcompany.model.Clientes;
import com.avanzar.avanzarcompany.model.Rol;
import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.ClientesService;
import com.avanzar.avanzarcompany.service.RolService;
import com.avanzar.avanzarcompany.service.UsuariosService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class RegistroClienteController {

    @Autowired
    private UsuariosService usuarioService;

    @Autowired
    private RolService rolService;
    
    @Autowired
    private ClientesService clienteService;

    private Usuarios usuarios;
    
    private Rol rol;

    private Clientes clientes;

    private int idUser;
    
    private static final Logger log = LogManager.getLogger(UsuariosController.class);

    @GetMapping("/registroCliente/{id}")
    public String index(@PathVariable int id,Model model){
        usuarios = new Usuarios();
        idUser = id;
        usuarios = usuarioService.consultar(id);
        rol = rolService.consultar(usuarios.getRol().getId_rol());
        model.addAttribute(usuarios);
        model.addAttribute(rol);
        clientes = new Clientes();
        model.addAttribute("clientes",clientes);
        return "RegistroClientes";
    }

    @PostMapping("/registroClienteNuevo")
    public String regitroClienteNuevo(@ModelAttribute("clientes") Clientes clientes, Model model, RedirectAttributes redirectAttribute){
    	model.addAttribute("clientes",clientes);

        redirectAttribute
                .addFlashAttribute("mensaje", "Agregado correctamente")
                .addFlashAttribute("clase", "alert alert-success");

        clienteService.guardar(clientes);
        redirectAttribute
                .addFlashAttribute("mensaje", "Agregado correctamente")
                .addFlashAttribute("clase", "alert alert-success");
        return "redirect:/registroCliente/"+idUser;
    }


}

    