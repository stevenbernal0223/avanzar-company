package com.avanzar.avanzarcompany.controller;

import com.avanzar.avanzarcompany.model.Rol;
import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.RolService;
import com.avanzar.avanzarcompany.service.UsuariosService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class RegistroUsuarioController {

    @Autowired
    private UsuariosService usuarioService;
    
    @Autowired	
    private RolService rolService;

    

    Integer idUsuario;
    
    private Usuarios usuarios1;

    private Usuarios usuarios2;

    int idUser;

    private static final Logger log = LogManager.getLogger(UsuariosController.class);

    @GetMapping("/registroUsuario/{id}")
    public String index(@PathVariable int id,Model model, Model model2){
        usuarios1 = new Usuarios();
        usuarios1 = usuarioService.consultar(id);
        idUser = usuarios1.getId_Usuario();
        model.addAttribute(usuarios1);
        usuarios2 = new Usuarios();
        model2.addAttribute("usuarios2",usuarios2);
        return "RegistroUsuarios";
    }

    @PostMapping("/registroUsrNuevo")
    public String regitroNUsr(@ModelAttribute("usuarios") Usuarios usuarios, Model model, RedirectAttributes redirectAttribute){
        model.addAttribute("usuarios",usuarios);
        usuarioService.guardar(usuarios);
        redirectAttribute
                .addFlashAttribute("mensaje", "Agregado correctamente")
                .addFlashAttribute("clase", "alert alert-success");
        return "redirect:/registroUsuario/"+idUser;
    }
    
    @GetMapping("/consulta")
    public String consulta (Model model){
        Usuarios usuarios = new Usuarios();
        usuarios = usuarioService.consultar(idUsuario);
        Rol rol = rolService.consultar(usuarios.getRol().getId_rol());
        model.addAttribute(usuarios);
        model.addAttribute(rol);
        return "redirect:/consultaAdministradorV/"+usuarios.getId_Usuario();
    }
    


   }