package com.avanzar.avanzarcompany.controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.avanzar.avanzarcompany.model.Usuarios;
import com.avanzar.avanzarcompany.service.UsuariosService;

import java.util.List;

@RestController
@RequestMapping(value = "/usuarios")
public class UsuariosController {
	
	 @Autowired
	    private UsuariosService service;

	    private static final Logger log = LogManager.getLogger(UsuariosController.class);

	    @GetMapping("/listarUsuarios")
	    public List<Usuarios> listarUsuarios() {
	        List<Usuarios> usuarios = service.listar();
	        log.info(usuarios);
	        return usuarios;
	    }
	    
	  //?id=
	    @GetMapping("/consultarUsuarios")
	    public Usuarios consultarUsuarios(Integer id) {
	        Usuarios usuarios = service.consultar(id);
	        log.info(usuarios);
	        return usuarios;
	    }

	    @PostMapping("/crearUsuarios")
	    public void crearUsuarios(@RequestBody Usuarios usuarios){
	        service.guardar(usuarios);
	    }

	    @DeleteMapping("/eliminarUsuarios")
	    public void eliminarAfp(Integer id){
	        service.eliminar(id);
	    }
	    
	    @PutMapping("/actualizarUsuarios")
	    public String actualizarUsuarios
	    (int id,String telefonoc,
	    		String direccionc, String correoc, String usuarioc, String contraseñac) {
	    	Usuarios usuc = service.consultar(id);
	    	if(usuc == null) {
	    		return "El usuario ingresado no existe";
	    	}
	    	
	    	usuc.setTelefono(telefonoc);
	    	usuc.setDireccion_de_residencia(direccionc);
	    	usuc.setCorreo_electronico(correoc);
	    	usuc.setUsuario(usuarioc);
	    	usuc.setContraseña(contraseñac);
	    	service.guardar(usuc);
	    	return "El  usuario ha sido actualizado correctamente";
	    }
	   



}
