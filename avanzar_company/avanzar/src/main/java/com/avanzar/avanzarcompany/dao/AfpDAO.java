package com.avanzar.avanzarcompany.dao;


import com.avanzar.avanzarcompany.model.Afp;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AfpDAO extends CrudRepository<Afp,Integer> {
	
	@Transactional
    @Modifying
    @Query(value = "UPDATE Afp afp SET afp.telefono=?1 WHERE afp.id_afp=?2", nativeQuery = true)
	void actualizarAfp(String tel,int idUser);

}
