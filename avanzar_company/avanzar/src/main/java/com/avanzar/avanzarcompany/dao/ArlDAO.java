package com.avanzar.avanzarcompany.dao;

import com.avanzar.avanzarcompany.model.Arl;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ArlDAO extends CrudRepository<Arl,Integer> {
	
	 @Transactional
	    @Modifying
	    @Query(value = "UPDATE Arl arl SET arl.telefono=?1 WHERE arl.id_arl=?2", nativeQuery = true)
		void actualizarArl(String tel,int idUser);

}
