package com.avanzar.avanzarcompany.dao;

import com.avanzar.avanzarcompany.model.Caja;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface CajaDAO extends CrudRepository<Caja,Integer> {

	 @Transactional
	    @Modifying
	    @Query(value = "UPDATE Caja caja SET caja.telefono=?1 WHERE caja.id_caja=?2", nativeQuery = true)
		void actualizarCaja(String tel,int idUser);

}
