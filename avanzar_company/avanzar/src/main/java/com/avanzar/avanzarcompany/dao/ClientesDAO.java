package com.avanzar.avanzarcompany.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.avanzar.avanzarcompany.model.Clientes;

public interface ClientesDAO extends CrudRepository<Clientes, Integer>{

	 @Query(value = "select * from clientes where numero_de_documento = ?1", nativeQuery= true)
	 Clientes consultaCliente(String cedulaC);

	 @Transactional
	    @Modifying
	    @Query(value = "UPDATE Clientes clientes SET clientes.telefono=?1, clientes.correo_electronico =?2, clientes.direccion_de_residencia =?3 WHERE clientes.id_cliente=?4", nativeQuery = true)
		void actualizarCliente(String telefono, String correo, String direccion, int idCliente);

}
