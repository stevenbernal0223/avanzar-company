package com.avanzar.avanzarcompany.dao;

import com.avanzar.avanzarcompany.model.Eps;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;


public interface EpsDAO extends CrudRepository<Eps,Integer> {

    @Transactional
    @Modifying
    @Query(value = "UPDATE Eps eps SET eps.telefono=?1 WHERE eps.id_eps=?2", nativeQuery = true)
	void actualizarEps(String tel,int idUser);

}
