package com.avanzar.avanzarcompany.dao;

import org.springframework.data.repository.CrudRepository;

import com.avanzar.avanzarcompany.model.Planes;

public interface PlanesDAO extends CrudRepository<Planes, Integer> {

}
