package com.avanzar.avanzarcompany.dao;

import com.avanzar.avanzarcompany.model.Rol;
import org.springframework.data.repository.CrudRepository;


public interface RolDAO extends CrudRepository<Rol,Integer> {

}
