package com.avanzar.avanzarcompany.dao;

import org.springframework.data.repository.CrudRepository;

import com.avanzar.avanzarcompany.model.Tipo_de_documento;

public interface TipodeDocumentoDAO extends CrudRepository<Tipo_de_documento, Integer>  {

}
