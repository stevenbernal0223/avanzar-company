package com.avanzar.avanzarcompany.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.avanzar.avanzarcompany.model.Usuarios;

public interface UsuariosDAO extends CrudRepository<Usuarios, Integer> {

    @Query(value = "select * from usuarios where usuario = ?1 and contraseña = ?2", nativeQuery = true)
    Usuarios login(String username, String password);

    @Query(value = "select * from usuarios where numero_de_documento = ?1", nativeQuery= true)
    Usuarios consultaUsuario(String cedula);
    
    @Transactional
    @Modifying
    @Query(value = "UPDATE Usuarios usuarios SET usuarios.telefono=?1, usuarios.correo_electronico =?2, usuarios.direccion_de_residencia =?3, usuarios.usuario =?4, usuarios.contraseña =?5 WHERE usuarios.id_usuario=?6", nativeQuery = true)
	void actualizarUsuario(String telefono, String correo, String direccion, String user, String contraseña, int idUsuario);
}
