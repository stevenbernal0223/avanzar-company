package com.avanzar.avanzarcompany.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "afp")
public class Afp {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_afp")
	private int id_afp;
	@Column( name = "nombre_de_la_AFP" )
	private String nombre_de_la_AFP;
	@Column( name = "nit" )
	private String nit;
	@Column( name = "telefono")
	private String telefono;
	
	@OneToMany(mappedBy = "afp")
	public int getId_afp() {
		return id_afp;
	}

	public void setId_afp(int id_afp) {
		this.id_afp = id_afp;
	}

	public String getNombre_de_la_AFP() {
		return nombre_de_la_AFP;
	}

	public void setNombre_de_la_AFP(String nombre_de_la_AFP) {
		this.nombre_de_la_AFP = nombre_de_la_AFP;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}
}
