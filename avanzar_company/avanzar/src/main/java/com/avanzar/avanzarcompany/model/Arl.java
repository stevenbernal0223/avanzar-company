package com.avanzar.avanzarcompany.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "arl")
public class Arl {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column( name = "id_arl")
	private int id_arl;
	@Column ( name = "nombre_de_la_ARL")
	private String nombre_de_la_ARL;
	@Column( name = "nit")
	private String nit;
	@Column ( name = "telefono" )
	private String telefono;
	
	@OneToMany(mappedBy = "arl")
	
	
	public int getId_arl() {
		return id_arl;
	}
	public void setId_arl(int id_arl) {
		this.id_arl = id_arl;
	}
	public String getNombre_de_la_ARL() {
		return nombre_de_la_ARL;
	}
	public void setNombre_de_la_ARL(String nombre_de_la_ARL) {
		this.nombre_de_la_ARL = nombre_de_la_ARL;
	}
	public String getNit() {
		return nit;
	}
	public void setNit(String nit) {
		this.nit = nit;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}
	
	
	
	