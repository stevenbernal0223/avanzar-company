package com.avanzar.avanzarcompany.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "caja")
public class Caja {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_caja")
	private int id_caja;
	@Column ( name = "nombre_de_la_Caja")
	private String nombre_de_la_Caja;
	@Column ( name = "nit")
	private String nit;
	@Column ( name = "telefono")
	private String telefono;
	
	@OneToMany(mappedBy = "caja")
	
	
	public int getId_caja() {
		return id_caja;
	}
	public void setId_caja(int id_caja) {
		this.id_caja = id_caja;
	}
	public String getNombre_de_la_Caja() {
		return nombre_de_la_Caja;
	}
	public void setNombre_de_la_Caja(String nombre_de_la_Caja) {
		this.nombre_de_la_Caja = nombre_de_la_Caja;
	}
	public String getNit() {
		return nit;
	}
	public void setNit(String nit) {
		this.nit = nit;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	
	

}
