package com.avanzar.avanzarcompany.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "clientes")
public class Clientes {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Cliente", nullable = false, unique = true)
	private int id_Cliente;	
	@Column ( name = "numero_de_documento")
	private String numero_de_documento;
	@Column ( name = "lugar_de_expedicion")
	private String lugar_de_expedicion;
	@Column ( name = "nombres")
	private String nombres;
	@Column ( name = "apellidos")
	private String apellidos;
	@Column ( name = "telefono")
	private String telefono;
	@Column ( name = "Correo_electronico")
	private String correo_electronico;
	@Column ( name = "direccion_de_residencia")
	private String direccion_de_residencia;
	
	
	
	//todas son foraneas de aqui en adelante
	//foranea	

	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn(name = "id_tipo", referencedColumnName = "id_tipo")	
	private Tipo_de_documento tipo_de_documento;
	
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn(name = "id_plan", referencedColumnName = "id_plan")
	private Planes planes;
	
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn(name = "id_eps", referencedColumnName = "id_eps")
	private Eps eps;
	
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn(name = "id_arl", referencedColumnName = "id_arl")
	private Arl arl;
	
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn(name = "id_afp", referencedColumnName = "id_afp")
	private Afp afp;
	
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn(name = "id_caja", referencedColumnName = "id_caja")
	private Caja caja;
	
	
	
	public String getCorreo_electronico() {
		return correo_electronico;
	}
	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}
	
	public Tipo_de_documento getTipo_de_documento() {
		return tipo_de_documento;
	}
	public void setTipo_de_documento(Tipo_de_documento tipo_de_documento) {
		this.tipo_de_documento = tipo_de_documento;
	}

	public Planes getPlanes() {
		return planes;
	}

	public void setPlanes(Planes planes) {
		this.planes = planes;
	}

	public Eps getEps() {
		return eps;
	}
	public void setEps(Eps eps) {
		this.eps = eps;
	}
	public Arl getArl() {
		return arl;
	}
	public void setArl(Arl arl) {
		this.arl = arl;
	}
	public Afp getAfp() {
		return afp;
	}
	public void setAfp(Afp afp) {
		this.afp = afp;
	}
	public Caja getCaja() {
		return caja;
	}
	public void setCaja(Caja caja) {
		this.caja = caja;
	}
	public int getId_Cliente() {
		return id_Cliente;
	}
	public void setId_Cliente(int id_Cliente) {
		this.id_Cliente = id_Cliente;
	}

	public String getNumero_de_documento() {
		return numero_de_documento;
	}
	public void setNumero_de_documento(String numero_de_documento) {
		this.numero_de_documento = numero_de_documento;
	}
	public String getLugar_de_expedicion() {
		return lugar_de_expedicion;
	}
	public void setLugar_de_expedicion(String lugar_de_expedicion) {
		this.lugar_de_expedicion = lugar_de_expedicion;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccion_de_residencia() {
		return direccion_de_residencia;
	}
	public void setDireccion_de_residencia(String direccion_de_residencia) {
		this.direccion_de_residencia = direccion_de_residencia;
	}
}
	
	
	

