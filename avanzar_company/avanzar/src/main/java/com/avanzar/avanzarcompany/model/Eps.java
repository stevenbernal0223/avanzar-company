package com.avanzar.avanzarcompany.model;

import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "eps")
public class Eps {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_eps")
	private int id_eps;
	@Column( name = "nombre_de_la_EPS" )
	private String nombre_de_la_EPS;
	@Column( name = "nit" )
	private String nit;
	@Column( name = "telefono" )
	private String telefono;

	@OneToMany(mappedBy = "eps")

	public int getId_eps() {
		return id_eps;
	}

	public void setId_eps(int id_eps) {
		this.id_eps = id_eps;
	}

	public String getNombre_de_la_EPS() {
		return nombre_de_la_EPS;
	}

	public void setNombre_de_la_EPS(String nombre_de_la_EPS) {
		this.nombre_de_la_EPS = nombre_de_la_EPS;
	}

	public String getNit() {
		return nit;
	}

	public void setNit(String nit) {
		this.nit = nit;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
}
