package com.avanzar.avanzarcompany.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "planes")
public class Planes {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_plan", nullable = false, unique = true)
	private int id_plan;
	@Column
	private String tipo_de_plan;
	@Column
	private String servicios;
	
	@OneToMany(mappedBy = "planes")
	
	public int getId_plan() {
		return id_plan;
	}
	public void setId_plan(int id_plan) {
		this.id_plan = id_plan;
	}
	public String getTipo_de_plan() {
		return tipo_de_plan;
	}
	public void setTipo_de_plan(String tipo_de_plan) {
		this.tipo_de_plan = tipo_de_plan;
	}
	public String getServicios() {
		return servicios;
	}
	public void setServicios(String servicios) {
		this.servicios = servicios;
	}
	
	

}
