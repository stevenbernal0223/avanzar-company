package com.avanzar.avanzarcompany.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name = "rol")
public class Rol {
	
	@Id
	private int id_rol;
	@Column
	private String tipo_de_rol;
	
	@OneToMany(mappedBy = "rol")	
	
	public int getId_rol() {
		return id_rol;
	}
	public void setId_rol(int id_rol) {
		this.id_rol = id_rol;
	}
	public String getTipo_de_rol() {
		return tipo_de_rol;
	}
	public void setTipo_de_rol(String tipo_de_rol) {
		this.tipo_de_rol = tipo_de_rol;
	}

}
