package com.avanzar.avanzarcompany.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import javax.persistence.Table;

@Entity
@Table(name = "Tipo_de_documento")
public class Tipo_de_documento {
	
	@Id
	private int id_tipo;
	@Column
	private String tipo_de_documento;
	
	@OneToMany(mappedBy = "tipo_de_documento")	
	
	public int getId_tipo() {
		return id_tipo;
	}
	public void setId_tipo(int id_tipo) {
		this.id_tipo = id_tipo;
	}
	public String getTipo_de_documento() {
		return tipo_de_documento;
	}
	public void setTipo_de_documento(String tipo_de_documento) {
		this.tipo_de_documento = tipo_de_documento;
	}
	
	
	

}
