package com.avanzar.avanzarcompany.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;



@Entity
@Table(name = "usuarios")
public class Usuarios {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_Usuario", nullable = false, unique = true)
	private int id_Usuario;
	@Column ( name = "numero_de_documento")
	private String numero_de_documento;
	@Column ( name = "nombres")
	private String nombres;
	@Column ( name = "apellidos")
	private String apellidos;
	@Column ( name = "telefono")
	private String telefono;
	@Column ( name = "direccion_de_residencia")
	private String direccion_de_residencia;
	@Column ( name = "correo_electronico")
	private String correo_electronico;
	@Column ( name = "usuario")
	private String usuario;
	@Column ( name = "contraseña")
	private String contraseña;
	
	//foranea
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn(name = "id_rol", referencedColumnName = "id_rol")
	private Rol rol;
	
	//foranea
	@ManyToOne (cascade = CascadeType.MERGE)
	@JoinColumn(name = "id_tipo", referencedColumnName = "id_tipo")	
	private Tipo_de_documento tipo_de_documento;
	
	
	public Rol getRol() {
		return rol;
	}
	public void setRol(Rol rol) {
		this.rol = rol;
	}
	public int getId_Usuario() {
		return id_Usuario;
	}
	public void setId_Usuario(int id_Usuario) {
		this.id_Usuario = id_Usuario;
	}
	
	public String getNumero_de_documento() {
		return numero_de_documento;
	}
	public void setNumero_de_documento(String numero_de_documento) {
		this.numero_de_documento = numero_de_documento;
	}
	public String getNombres() {
		return nombres;
	}
	public void setNombres(String nombres) {
		this.nombres = nombres;
	}
	public String getApellidos() {
		return apellidos;
	}
	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getDireccion_de_residencia() {
		return direccion_de_residencia;
	}
	public void setDireccion_de_residencia(String direccion_de_residencia) {
		this.direccion_de_residencia = direccion_de_residencia;
	}
	public String getCorreo_electronico() {
		return correo_electronico;
	}
	public void setCorreo_electronico(String correo_electronico) {
		this.correo_electronico = correo_electronico;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getContraseña() {
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	public Tipo_de_documento getTipo_de_documento() {
		return tipo_de_documento;
	}
	public void setTipo_de_documento(Tipo_de_documento tipo_de_documento) {
		this.tipo_de_documento = tipo_de_documento;
	}
	
	
	
	
		
}
