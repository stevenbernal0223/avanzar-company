package com.avanzar.avanzarcompany.service;

import com.avanzar.avanzarcompany.commons.GenericService;
import com.avanzar.avanzarcompany.model.Afp;

public interface AfpService extends GenericService<Afp, Integer> {
	
	public void actualizarAfp(String telefono, int id);
	
}
