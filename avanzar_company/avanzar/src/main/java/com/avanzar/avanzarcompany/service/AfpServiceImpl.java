package com.avanzar.avanzarcompany.service;

import com.avanzar.avanzarcompany.commons.GenericServiceImpl;
import com.avanzar.avanzarcompany.dao.AfpDAO;
import com.avanzar.avanzarcompany.model.Afp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

@Service
public class AfpServiceImpl  extends GenericServiceImpl<Afp, Integer> implements AfpService{

    //extender es heredar y implements es conectar

    @Autowired
    private AfpDAO afpDao;
    
    public void actualizarAfp (String telefono, int id) {
    	afpDao.actualizarAfp(telefono, id);
    }
    

    @Override
    public CrudRepository<Afp, Integer> getDao() {
        return afpDao;
    }



    
    
}
