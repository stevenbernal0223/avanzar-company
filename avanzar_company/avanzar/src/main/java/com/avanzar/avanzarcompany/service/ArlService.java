package com.avanzar.avanzarcompany.service;

import com.avanzar.avanzarcompany.commons.GenericService;
import com.avanzar.avanzarcompany.model.Arl;

public interface ArlService extends GenericService<Arl, Integer>{

	public void actualizarArl(String telefono, int id);

}
