package com.avanzar.avanzarcompany.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;

import com.avanzar.avanzarcompany.commons.GenericServiceImpl;
import com.avanzar.avanzarcompany.dao.ArlDAO;
import com.avanzar.avanzarcompany.model.Arl;

@Service
public class ArlServiceImpl extends GenericServiceImpl<Arl, Integer> implements ArlService{
	
	@Autowired
    private ArlDAO arlDao;
	
	public void actualizarArl(String telefono, int id) {
		arlDao.actualizarArl(telefono, id);
	}
	
    @Override
    public CrudRepository<Arl, Integer> getDao() {
        return arlDao;
    }

	
	

	
}
