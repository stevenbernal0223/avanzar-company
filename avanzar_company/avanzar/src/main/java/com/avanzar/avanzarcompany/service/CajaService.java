package com.avanzar.avanzarcompany.service;

import com.avanzar.avanzarcompany.commons.GenericService;
import com.avanzar.avanzarcompany.model.Caja;

public interface CajaService extends GenericService<Caja, Integer> {
	
public void actualizarCaja(String tel,int idUser);

}
