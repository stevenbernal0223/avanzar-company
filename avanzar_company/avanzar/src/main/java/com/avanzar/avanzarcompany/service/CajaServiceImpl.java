package com.avanzar.avanzarcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.avanzar.avanzarcompany.commons.GenericServiceImpl;
import com.avanzar.avanzarcompany.dao.CajaDAO;
import com.avanzar.avanzarcompany.model.Caja;

@Service
public class CajaServiceImpl extends GenericServiceImpl<Caja, Integer> implements CajaService {
	
	@Autowired
    private CajaDAO cajaDao;
	
	public void actualizarCaja(String tel,int idUser) {
		cajaDao.actualizarCaja(tel, idUser);
	}


    @Override
    public CrudRepository<Caja, Integer> getDao() {
        return cajaDao;
    }

}
