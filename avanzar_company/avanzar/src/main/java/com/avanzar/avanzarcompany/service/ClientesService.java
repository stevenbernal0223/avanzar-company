package com.avanzar.avanzarcompany.service;

import com.avanzar.avanzarcompany.commons.GenericService;
import com.avanzar.avanzarcompany.model.Clientes;

public interface ClientesService extends GenericService<Clientes, Integer> {
	
	public Clientes consultaClientes(String cedulaC);
	
	public void actualizarCliente(String telefono, String correo, String direccion, int id);


}
