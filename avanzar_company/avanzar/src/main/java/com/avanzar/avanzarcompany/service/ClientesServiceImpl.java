package com.avanzar.avanzarcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.avanzar.avanzarcompany.commons.GenericServiceImpl;
import com.avanzar.avanzarcompany.dao.ClientesDAO;
import com.avanzar.avanzarcompany.model.Clientes;

@Service
public class ClientesServiceImpl extends GenericServiceImpl<Clientes, Integer> implements ClientesService {
	
	@Autowired
	private ClientesDAO clientesDao;
	
	@Override
	public Clientes consultaClientes(String cedulaC) {
		Clientes cliente = clientesDao.consultaCliente(cedulaC);
		return cliente;
	}
	
	public void actualizarCliente(String telefono, String correo, String direccion, int id) {
		clientesDao.actualizarCliente(telefono, correo, direccion, id);
	}

	




	@Override
	public CrudRepository<Clientes, Integer> getDao() {
		return clientesDao;
	}

	
}
