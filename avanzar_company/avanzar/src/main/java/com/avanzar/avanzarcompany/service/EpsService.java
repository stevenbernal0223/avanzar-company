package com.avanzar.avanzarcompany.service;

import com.avanzar.avanzarcompany.commons.GenericService;
import com.avanzar.avanzarcompany.model.Eps;

public interface EpsService extends GenericService<Eps, Integer> {

    public void actualizarEps(String telefono, int id);
}
