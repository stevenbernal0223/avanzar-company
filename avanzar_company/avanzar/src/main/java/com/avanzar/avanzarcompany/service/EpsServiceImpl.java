package com.avanzar.avanzarcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.avanzar.avanzarcompany.commons.GenericServiceImpl;
import com.avanzar.avanzarcompany.dao.EpsDAO;
import com.avanzar.avanzarcompany.model.Eps;

@Service
public class EpsServiceImpl extends GenericServiceImpl<Eps, Integer> implements EpsService{
	
	@Autowired
    private EpsDAO epsDao;

    public void actualizarEps(String telefono, int id ){
        epsDao.actualizarEps(telefono, id);
    }
	
	@Override
    public CrudRepository<Eps, Integer> getDao() {
        return epsDao;
    }

}
