package com.avanzar.avanzarcompany.service;

import com.avanzar.avanzarcompany.commons.GenericService;
import com.avanzar.avanzarcompany.model.Planes;

public interface PlanesService extends GenericService<Planes, Integer> {

}
