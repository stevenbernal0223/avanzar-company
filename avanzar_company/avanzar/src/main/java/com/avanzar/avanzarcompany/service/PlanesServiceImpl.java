package com.avanzar.avanzarcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.avanzar.avanzarcompany.commons.GenericServiceImpl;
import com.avanzar.avanzarcompany.dao.PlanesDAO;
import com.avanzar.avanzarcompany.model.Planes;

@Service
public class PlanesServiceImpl extends GenericServiceImpl<Planes, Integer> implements PlanesService{
	
	@Autowired
	private PlanesDAO planesDao;
	
	@Override
	public CrudRepository<Planes, Integer> getDao(){
		return planesDao;
	}

}
