package com.avanzar.avanzarcompany.service;

import com.avanzar.avanzarcompany.commons.GenericServiceImpl;
import com.avanzar.avanzarcompany.dao.RolDAO;
import com.avanzar.avanzarcompany.model.Rol;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;


@Service
public class RolServiceImpl extends GenericServiceImpl<Rol, Integer> implements RolService {
	
	@Autowired
	private RolDAO rolDao;
	
	@Override
	public CrudRepository<Rol, Integer> getDao() {
		return rolDao ;
	}

}
