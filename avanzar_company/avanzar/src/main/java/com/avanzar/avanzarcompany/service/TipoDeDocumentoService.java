package com.avanzar.avanzarcompany.service;

import com.avanzar.avanzarcompany.commons.GenericService;
import com.avanzar.avanzarcompany.model.Tipo_de_documento;

public interface TipoDeDocumentoService extends GenericService<Tipo_de_documento, Integer> {

}
