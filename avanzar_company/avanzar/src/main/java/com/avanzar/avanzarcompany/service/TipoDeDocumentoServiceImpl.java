package com.avanzar.avanzarcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import com.avanzar.avanzarcompany.commons.GenericServiceImpl;
import com.avanzar.avanzarcompany.dao.TipodeDocumentoDAO;
import com.avanzar.avanzarcompany.model.Tipo_de_documento;

@Service
public class TipoDeDocumentoServiceImpl extends GenericServiceImpl<Tipo_de_documento, Integer> implements TipoDeDocumentoService  {
	
	@Autowired
	private TipodeDocumentoDAO tipoDeDocumentoDao;
	
	@Override
	public CrudRepository<Tipo_de_documento, Integer> getDao() {
		return tipoDeDocumentoDao ;
	}

}
