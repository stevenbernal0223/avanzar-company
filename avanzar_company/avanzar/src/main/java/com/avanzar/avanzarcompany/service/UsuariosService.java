package com.avanzar.avanzarcompany.service;

import com.avanzar.avanzarcompany.commons.GenericService;
import com.avanzar.avanzarcompany.model.Usuarios;

public interface UsuariosService extends GenericService<Usuarios, Integer> {

    public Usuarios consultaLogin(String usuario, String password);
    
    public Usuarios consultaUsuario(String cedula);

	void actualizarUsuario(String telefono, String correo, String direccion, String user, String contraseña, int id);


}
