	package com.avanzar.avanzarcompany.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;
import com.avanzar.avanzarcompany.commons.GenericServiceImpl;
import com.avanzar.avanzarcompany.dao.UsuariosDAO;
import com.avanzar.avanzarcompany.model.Usuarios;

@Service
public class UsuariosServiceImpl extends GenericServiceImpl<Usuarios, Integer> implements UsuariosService{
	
	@Autowired
    private UsuariosDAO usuariosDao;


	public Usuarios consultaLogin(String usuario, String password){
	    Usuarios user = usuariosDao.login(usuario,password );
	    return user;
	}
	
	public 	void actualizarUsuario(String telefono, String correo, String direccion, String user, String contraseña, int id) {
		usuariosDao.actualizarUsuario(telefono, correo, direccion, user, contraseña, id);
	}

	
	@Override
	public Usuarios consultaUsuario(String cedula) {
		Usuarios usuario = usuariosDao.consultaUsuario(cedula);
		return usuario;
	}


    @Override
    public CrudRepository<Usuarios, Integer> getDao() {
        return usuariosDao;
    }


	

}
